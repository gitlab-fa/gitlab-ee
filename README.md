---
toc: false
---

# مستندات گیت‌لب 

به [گیت‌لب](https://about.gitlab.com/) خوش آمدید، پلتفرمی با امکانات کامل بر پایه‌ی 
Git که برای توسعه‌ی نرم‌افزار ایجاد شده است!

گیت‌لب قابل قیاس ترین پلتفرم یکپارچه‌ی بر پایه‌ی Git را برای توسعه‌ی نرم‌افزار را به همراه محصولات و طرح‌های عضویت انعطاف پذیر ارائه می‌دهد؛

- نسخه‌ی جامعه‌ی گیت‌لب {**GitLab Community Edition (CE)**}، یک [محصول متن‌باز{opensource product}](https://gitlab.com/gitlab-org/gitlab-ce/) است، 
خود میزبان{self-hosted}، رایگان برای استفاده. هر ویژگی موجود در GitLab CE نیز در نسخه‌ی تجاری گیت‌لب{GitLab Enterprise Edition} (Starter و Premium) و GitLab.com موجود است.
- نسخه‌ی تجاری گیت‌لب {**GitLab Enterprise Edition (EE)**}، یک محصول [هسته‌باز{opencore}](https://gitlab.com/gitlab-org/gitlab-ee/) است، 
خود میزبان{self-hosted}، با تمامی امکانات گیت‌لب، تحت [اشتراک‌های](https://about.gitlab.com/products/) مجزا موجود است: GitLab Enterprise Edition Starter (EES) و نسخه‌ی ویژه‌ی تجاری گیت‌لب (EEP)
- **GitLab.com**: راه‌حل نرم‌افزار به عنوان یک سرویس{SaaS} با [اشتراک‌های رایگان و پولی](https://about.gitlab.com/gitlab-com/). GitLab.com توسط شرکت GitLab, inc.، میزبانی می‌شود و توسط GitLab مدیریت می‌شود(کاربران به تنظیم مدیریت دسترسی ندارند)

> **GitLab EE** شامل تمام امکانات در دسترس در **GitLab CE** است،
به علاوه‌ی امکانات ویژه‌ای که در هر نسخه وجود دارد: **Enterprise Edition Starter**
**(EES)** و **نسخه‌ی ویژه‌ی تجاری گیت‌لب **(EEP)**. همچنین هرچه در
**EES** موجود است در **EEP** هم وجود دارد.

----

میانبرهایی برای دسترسی به پربازدیدترین مستندات گیت‌لب:

| [GitLab CI](ci/README.md) | Other |
| :----- | :----- |
| [Quick start guide](ci/quick_start/README.md) | [API](api/README.md) |
| [Configuring `.gitlab-ci.yml`](ci/yaml/README.md) | [SSH authentication](ssh/README.md) |
| [Using Docker images](ci/docker/using_docker_images.md) | [GitLab Pages](user/project/pages/index.md) |

- [User documentation](user/index.md)
- [Administrator documentation](#administrator-documentation)
- [Technical Articles](articles/index.md)

## شروع کار با گیت‌لب

- [موارد پایه گیت‌لب](gitlab-basics/README.md): شروع کار با خط فرمان و گیت‌لب
- [روند اجرای کار گیت‌لب {GitLab Workflow}](workflow/README.md): روند اجرا و گردش‌کار خود را با استفاده از بهترین GitLab Workflow بهبود ببخشید.
	- همچنین نگاهی به [روند اجرای کار گیت‌لب](https://about.gitlab.com/2016/10/25/gitlab-workflow-an-overview/) بیندازید.
- [نشانه‌گذاری گیت‌لب](user/markdown.md): سیستم پیشرفته‌ی قالب‌بندی گیت‌لب (نشانه‌گذاری با طعم گیت‌لب).
- [اقدامات سریع گیت‌لب {GitLab Quick Actions}](user/project/quick_actions.md): میانبرهای متنی برای اقدامات رایج در مورد مسائل و یا درخواست های ادغام که معمولا با کلیک کردن بر روی دکمه‌ها یا کشویی‌ها در گیت‌لب UI انجام می‌شود.

### حساب کاربری

- [حساب کاربری](user/profile/index.md): حسابتان را مدیریت کنید
	- [احراز هویت](topics/authentication/index.md): امنیت حساب با احراز هویت دو مرحله‌ای، کلید‌های SSH خود را راه‌اندازی کنید و کلید‌ها را برای دسترسی امن به پروژه‌های خود اعمال کنید.
	- [تنظیمات نمایه](user/profile/index.md#profile-settings): تنظیمات نمایه خود را با احراز هویت دو مرحله و چیز های بیشتر مدیریت کنید.
- [دسترسی‌های کاربر](user/permissions.md): یاد بگیرید که هر نقش در یک پروژه (خارجی{external}/مهمان{guest}/گزارش‌دهنده{reporter}/توسعه‌دهنده{developer}/رییس{master}/مالک{owner}) چه می‌تواند بکند.

### پروژه‌ها و گروه‌ها

- [پروژه‌ها](user/project/index.md):
	- [یک پروژه ایجاد کنید](gitlab-basics/create-project.md)
	- [یک پروژه را فورک کنید](gitlab-basics/fork-project.md)
	- [نمونه‌هایی از پروژه‌ها را درونریزی یا برونریزی کنید](user/project/settings/import_export.md).
	- [دسترسی به پروژه](public_access/public_access.md): تنظیم میزان اشکار بودن پروژه هایتان در سه سطح همگانی, اینترنت و خصوصی
	- [صفحات گیت‌لب](user/project/pages/index.md): بسازید، امتحان کنید و وبسایت‌های ایستایی خودتون رو با صفحات گیت‌لب پیاده‌سازی کنید.
- [گروه‌ها](user/group/index.md): پروژه‌هایتان را در گروه‌ها سازماندهی کنید.
	- [زیر‌گروه‌ها](user/group/subgroups/index.md)
- [از توی گیت‌لب جستجو کنید](user/search/index.md): جستجو برای مسائل، درخواست‌های ادغام، پروژه‌ها، گروه‌ها، هدف‌ها{todos} و مسائل در تخته‌ی مسائل{Issue Boards}.
	- **(EES/EEP)** [جستجوی عمومی پیشرفته‌](user/search/advanced_global_search.md): Leverage Elasticsearch برای جستجوی سریع‌تر و پیشرفته‌تر میان کد‌های تمتم نمونه‌ی گیت‌لب شما.
	- **(EES/EEP)** [سرچ پیشرفته ی سینتکس](user/search/advanced_search_syntax.md): از جستار‌های پیشرفته برای جستجو‌های هدف‌دار‌تر استفاده کنید.
- [قطعه‌ها](user/snippets.md): قطعه‌ها به شما اجازه میدهند تیکه‌های کوچک کد ایجاد کنید.
- [ویکی‌ها](user/project/wiki/index.md): مستندات مخازنتون رو با ویکی های ساخته شده بهبود ببخشید.
- [صفحات گیت‌لب](user/project/pages/index.md): بسازید، امتحان کنید و وبسایت‌های ایستایی خودتون رو با صفحات گیت‌لب پیاده‌سازی کنید.
- **(EEP)** [میز سرویس گیت‌لب](user/project/service_desk.md): یک راه ساده برای اجازه دادن به مردم برای ایجاد مسائل در نمونه گیت‌لب شما بدون داشتن حساب.
- **(EES/EEP)** [تجزیه و تحلیل مشارکت](analytics/contribution_analytics.md): مشاهده آمار دقیق مشارکت‌کنندگان پروژه‌ها.

### مخزن

[مخازن](user/project/repository/index.md) خود را با UI (رابط کاربری) مدیریت کنید:

- پرونده‌ها
	- [یک پرونده بسازید](user/project/repository/web_editor.md#create-a-file)
	- [یک پرونده بارگذاری کنید](user/project/repository/web_editor.md#upload-a-file)
	- [قالب پرونده](user/project/repository/web_editor.md#template-dropdowns)
	- [یک دایرکتوری بسازید](user/project/repository/web_editor.md#create-a-directory)
	- [یک درخواست ادغام را شروع کنید](user/project/repository/web_editor.md#tips) (وقتی که توسط UI کامیت می‌کنید)
	- [**(EES/EEP)** یک پرونده را قفل کنید](user/project/file_lock.md): یک پرونده را قفل کنید تا از تداخل هنگام ادغام جلوگیری کنید.
- شاخه‌ها
	- [یک شاخه بسازید](user/project/repository/web_editor.md#create-a-new-branch)
	- [شاخه‌های حفاظت شده](user/project/protected_branches.md#protected-branches)
	- [شاخه‌های ادغام شده را حذف کنید](user/project/repository/branches/index.md#delete-merged-branches)
- **(EES/EEP)** [بازتاب کردن مخازن](workflow/repository_mirroring.md)
- **(EES/EEP)** [قاعده وارد‌کردن{push}](push_rules/push_rules.md):کنترلی اضافه بر روی وارد‌کردن‌هایی که روی پروژه شما انجام می‌شود.
- **(EEP)** [قفل پروند‌ها](user/project/file_lock.md): یک پرونده را قفل کنید تا از تداخل هنگام ادغام جلوگیری کنید.



